package com.example.homework_spring;

public class PostCustomer {
    private String name;
    private String gender;
    private Integer age;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public PostCustomer(String name, String gender, Integer age, String address) {
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
    }
}
