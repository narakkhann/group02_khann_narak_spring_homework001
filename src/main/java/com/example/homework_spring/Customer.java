package com.example.homework_spring;

import com.fasterxml.jackson.annotation.JsonIgnore;
public class Customer {
    private Integer id;
    private String name;
    private String gender;
    private Integer age;
    private String address;

    public Customer(Integer id,String name, String gender, Integer age, String address) {
        this.id=id;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
    }
    public Customer(){}

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
