package com.example.homework_spring;

import java.time.LocalDateTime;

public class ApiResponse<T> {
    private String message;
    private T customer;
    private String status;
    private LocalDateTime time;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    public T getCustomer() {
        return customer;
    }
    public void setCustomer(T customer) {
        this.customer = customer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public ApiResponse(String message, T customer, String status, LocalDateTime time) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.time = time;
    }
}