package com.example.homework_spring;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
@RestController
public class ControllerCustomer {
    ArrayList<Customer> customer = new ArrayList<>();
    public ControllerCustomer(){
        customer.add(new Customer(1,"Khann Narak","Male",21,"KPC"));
        customer.add(new Customer(2,"Thol Kanchana","Female",16,"TBK"));
        customer.add(new Customer(3,"Pech Kimheang","Male",21,"PP"));
    }
    @GetMapping("/api/v1/customers")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new ApiResponse<ArrayList<Customer>>(
                "You got all customer successfully",
                 customer,
                "OK",
                LocalDateTime.now()
        ));
    }
    int autoId;
    @PostMapping("/api/v1/customers")
    public ResponseEntity<ApiResponse<Customer>> InsertCustomer(@RequestBody PostCustomer postCustomer){
        autoId=customer.size()+1;
        customer.add(new Customer(autoId,postCustomer.getName(),postCustomer.getGender(),postCustomer.getAge(),postCustomer.getAddress()));
        Customer newCustomer= new Customer();
        for(Customer cust : customer){
            if(cust.getId().equals(autoId)){
                newCustomer=cust;
                break;
            }
        }
        return ResponseEntity.ok(new ApiResponse<>(
                "This record was successfully created",
                newCustomer,
                "OK",
                LocalDateTime.now()
        )) ;
    }
    @GetMapping("/api/v1/customers/{customerId}")
    public ResponseEntity<?> getCustomerById(@PathVariable("customerId") Integer customerId){
        for(Customer cust : customer){
            if(cust.getId() == customerId){
                return ResponseEntity.ok(new ApiResponse<Customer>(
                        "This record has found successfully",
                        cust,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }
    @GetMapping("/api/v1/customers/search")
    public ResponseEntity<ApiResponse<Customer>> getCustomerByName(@RequestParam String search){
        for(Customer cust : customer){
            if(cust.getName().equals(search)){
                return ResponseEntity.ok(new ApiResponse<Customer>(
                        "This record has found successfully",
                        cust,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }
    @PutMapping("/api.v1/customers/{customerId}")
    public ResponseEntity<?> UpdateCustomer(@PathVariable("customerId") Integer customerId, @RequestBody PostCustomer postCustomer){
        for(Customer cust : customer){
            if(cust.getId().equals(customerId)){
                cust.setName(postCustomer.getName());
                cust.setAge(postCustomer.getAge());
                cust.setGender(postCustomer.getGender());
                cust.setAddress(postCustomer.getAddress());
                return ResponseEntity.ok(new ApiResponse<>(
                        "This record has updated successfully",
                        cust,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/api.v1/customers/{customerId}")
    public ResponseEntity<?> DeleteCustomer(@PathVariable("customerId") Integer CustomerId){
        for(Customer cust : customer){
            if(cust.getId().equals(CustomerId)){
                customer.remove(cust);
                return ResponseEntity.ok(new ApiResponse<>(
                        "This record has Deleted successfully",
                        cust,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }
}
